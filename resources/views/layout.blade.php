<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Khabariya News" />
        <title>@yield('title' , 'Khabariyanews')</title>
        <meta property="description" content="Khabariya news is a news web portal. Get the latest updated news from around the globe. ">
        <!-- Open Graph / Facebook -->
        <meta property="og:type" content="website">
        <meta property="og:url" content="http://khabariyanews.com/">
        <meta property="og:title" content="Khabariya News">
        <meta property="og:description" content="Khabariya news is a news web portal. Get the latest updated news from around the globe. ">
        <meta property="og:image" content="{{ @asset('img/logo.png') }}">

        <!-- Twitter -->
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:url" content="http://khabariyanews.com/">
        <meta property="twitter:title" content="Khabariya News">
        <meta property="twitter:description" content="Khabariya news is a news web portal. Get the latest updated news from around the globe. ">
        <meta property="twitter:image" content="{{ @asset('img/logo.png') }}">

        <link rel="apple-touch-icon" sizes="57x57" href="{{@asset('img/apple-icon-57x57.png')}}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ @asset('img/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ @asset('img/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ @asset('img/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ @asset('img/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ @asset('img/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ @asset('img/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ @asset('img/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ @asset('img/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ @asset('img/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ @asset('img/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ @asset('img/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ @asset('img/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ @asset('img/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ @asset('img//ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">
        <link rel="shortcut icon" href="{{ @asset('img/favicon.ico')}}" />
        <link href="{{ @asset('img/favicon.ico')}}" rel="icon">
        <link href="{{ @asset("css/bootstrap.min.css") }}" rel="stylesheet">
        <link href="{{ @asset("css/animsition.min.css") }}" rel="stylesheet">
        <link href="{{ @asset("css/font-awesome.min.css") }}" rel="stylesheet">
        <link href="{{ @asset("themify-icons/themify-icons.css") }}" rel="stylesheet">
        <link href="{{ @asset("css/bootsnav.css") }}" rel="stylesheet">
        <link href="{{ @asset("owl-carousel/owl.carousel.css") }}" rel="stylesheet">
        <link href="{{ @asset("owl-carousel/owl.theme.css") }}" rel="stylesheet">
        <link href="{{ @asset("owl-carousel/owl.transitions.css") }}" rel="stylesheet">
        <link href="{{ @asset("css/magnific-popup.css") }}" rel="stylesheet">
        <link href="{{ @asset("css/fluidbox.min.css") }}" rel="stylesheet">
        <link href="{{ @asset("css/style.css") }}" rel="stylesheet">
    </head>
    <body>
        <div class="main-content animsition">
            @include('includes.topbar')
            @include('includes.navbar')
            @include('includes.newsticker')
            @yield('slider')
            @yield('content')
            @include('includes.footer')
        </div>

    </body>
    
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script data-cfasync="false" src="../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="{{ @asset("/js/jquery.min.js") }}"></script>
        <script src="{{ @asset("/js/bootstrap.min.js") }}"></script>
        <script src="{{ @asset("/js/animsition.min.js") }}"></script>
        <script src="{{ @asset("/js/bootsnav.js") }}"></script>
        <script src="{{ @asset("/js/macy.js") }}"></script>
        <script src="{{ @asset("/js/imagesloaded.pkgd.min.js") }}"></script>
        <script src="{{ @asset("/js/ResizeSensor.min.js") }}"></script>
        <script src="{{ @asset("/js/theia-sticky-sidebar.min.js") }}"></script>
        <script src="{{ @asset("/js/jquery.magnific-popup.min.js") }}"></script>
        <script src="{{ @asset("/owl-carousel/owl.carousel.min.js") }}"></script>
        <script src="{{ @asset("/js/modernizr.custom.js") }}"></script>
        <script src="{{ @asset("/js/jquery.gridrotator.min.js") }}"></script>
        <script src="{{ @asset("/js/parallax-background.min.js") }}"></script>
        <script src="{{ @asset("/js/jquery.simpleSocialShare.min.js") }}"></script>
        <script src="{{ @asset("/js/jquery.fluidbox.min.js") }}"></script>
        <script src="{{ @asset("/js/retina.min.js") }}"></script>
        <script src="{{ @asset("/js/jquery.shuffle.min.js") }}"></script>
        <script src="{{ @asset("/js/readingTime.min.js") }}"></script>
        <script src="{{ @asset("/js/custom.js") }}"></script>
        <script>
            $(document).ready(function () {
                'use strict';
                //Grid content 
                var masonry = new Macy({
                    container: '.grid-content',
                    trueOrder: false,
                    waitForImages: false,
                    useOwnImageLoader: false,
                    debug: true,
                    mobileFirst: true,
                    columns: 1,
                    margin: 30,
                    breakAt: {
                        1200: 2,
                        992: 2,
                        768: 2,
                        480: 2
                    }
                });
            });
        </script>
</html>