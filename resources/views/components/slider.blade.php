<div class="news-masonry">
    <div class="container">
        <div class="row mas-m">
            <div class="col-sm-6 mas-p">
                <div class="row mas-m">
                    <div class="col-xs-6 col-sm-6 mas-p">
                        <div class="masonry-slide1 owl-carousel owl-theme">
                            @foreach (array_splice($sliders, 0 , 2) as $news)
                            <?php 
                                $newsID = $news['id'];
                                $newsSlug = $news['slug'];
                                $catID = $news['category']['id'];
                                $catSlug = $news['category']['category_slug'];
                            ?>
                                    <div class="item mas-m-b">
                                        <div class="mas-item masonry-sm">
                                            <a href="/news/{{$newsID}}/{{$newsSlug}}">
                                                <figure>
                                                    <img src="{{ @asset($news['imgUrl'] ) }}" class="img-responsive" alt="{{ $news['title'] }}" >
                                                </figure>
                                            </a>
                                            <div class="mas-text">
                                                <div class="post-cat">
                                                    <a href="/category/{{$catID}}/{{$catSlug}}">{{$news['category']['category_name']}}</a>
                                                </div>
                                                
                                                <h5 class="mas-title"><a href="/news/{{$newsID}}/{{$newsSlug}}">{{ $news['title'] }}</a></h5>
                                                <div class="mas-details">
                                                    <a href="/news/{{$newsID}}/{{$newsSlug}}" class="read-more">Read More &#8702;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               
                            @endforeach
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 mas-p">
                        <div class="masonry-slide2 owl-carousel owl-theme">
                            @foreach (array_splice($sliders, 2 , 4) as $news)
                            <div class="item">
                                <div class="mas-item masonry-sm">
                                    <a href="#"><figure><img src="{{ @asset("img/masonry/282x223-03.jpg") }}" class="img-responsive" alt=""></figure></a>
                                    <div class="mas-text">
                                        <div class="post-cat"><a href="#">Fashion</a></div>
                                        <h4 class="mas-title"><a href="#">It is a long <em>established</em> fact that a reader</a></h4>
                                        <div class="mas-details">
                                            <p>There are many variations of passages of Lorem Ipsum available</p>
                                            <a href="#" class="read-more">Read More &#8702;</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.End of masonry item -->
                            </div>
                               
                            @endforeach
                           
                        </div>
                    </div>
                </div>
                <div class="hidden-xs row mas-m">
                    <div class="col-sm-6 mas-p">
                        <div class="masonry-slide3 owl-carousel owl-theme">
                            @foreach (array_splice($sliders, 4 , 6) as $news)
                            <div class="item">
                                <div class="mas-item masonry-sm">
                                    <a href="#"><figure><img src="{{ @asset("img/masonry/282x223-03.jpg") }}" class="img-responsive" alt=""></figure></a>
                                    <div class="mas-text">
                                        <div class="post-cat"><a href="#">Fashion</a></div>
                                        <h4 class="mas-title"><a href="#">It is a long <em>established</em> fact that a reader</a></h4>
                                        <div class="mas-details">
                                            <p>There are many variations of passages of Lorem Ipsum available</p>
                                            <a href="#" class="read-more">Read More &#8702;</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.End of masonry item -->
                            </div>
                               
                            @endforeach
                           
                        </div>
                    </div>
                    <div class="col-sm-6 mas-p">
                        <div class="masonry-slide4 owl-carousel owl-theme">
                            @foreach (array_splice($sliders, 6 ,8) as $news)
                            <div class="item">
                                <div class="mas-item masonry-sm">
                                    <a href="#"><figure><img src="{{ @asset("img/masonry/282x223-03.jpg") }}" class="img-responsive" alt=""></figure></a>
                                    <div class="mas-text">
                                        <div class="post-cat"><a href="#">Fashion</a></div>
                                        <h4 class="mas-title"><a href="#">It is a long <em>established</em> fact that a reader</a></h4>
                                        <div class="mas-details">
                                            <p>There are many variations of passages of Lorem Ipsum available</p>
                                            <a href="#" class="read-more">Read More &#8702;</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.End of masonry item -->
                            </div>
                               
                            @endforeach
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 mas-p">
                <div class="row mas-m">
                    <div class="col-xs-6 col-sm-6 mas-p">
                        <div class="masonry-slide1 owl-carousel owl-theme">
                            @foreach (array_splice($sliders, 8 , 10) as $news)
                              
                            <div class="item mas-m-b">
                                <div class="mas-item masonry-sm">
                                    <a href="#">
                                        <figure>
                                            <img src="{{ @asset($news['imgUrl'] ) }}" class="img-responsive" alt="{{ $news['title'] }}" >
                                        </figure>
                                    </a>
                                    <div class="mas-text">
                                        <div class="post-cat">
                                            <a href="">{{$news['category']['category_name']}}</a>
                                        </div>
                                        <h5 class="mas-title"><a href="/news/">{{ $news['title'] }}</a></h5>
                                        <div class="mas-details">
                                            <a href="/news/" class="read-more">Read More &#8702;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                       
                    @endforeach
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 mas-p">
                        <div class="masonry-slide2 owl-carousel owl-theme">
                            @foreach (array_splice($sliders, 10 , 12) as $news)
                              
                                    <div class="item mas-m-b">
                                        <div class="mas-item masonry-sm">
                                            <a href="#">
                                                <figure>
                                                    <img src="{{ @asset($news['imgUrl'] ) }}" class="img-responsive" alt="{{ $news['title'] }}" >
                                                </figure>
                                            </a>
                                            <div class="mas-text">
                                                <div class="post-cat">
                                                    <a href="">{{$news['category']['category_name']}}</a>
                                                </div>
                                                <h5 class="mas-title"><a href="/news/">{{ $news['title'] }}</a></h5>
                                                <div class="mas-details">
                                                    <a href="/news/" class="read-more">Read More &#8702;</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                               
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="hidden-xs row mas-m">
                    <div class="col-sm-6 mas-p">
                        <div class="masonry-slide3 owl-carousel owl-theme">
                            @foreach (array_splice($sliders, 12 , 14) as $news)
                            <div class="item">
                                <div class="mas-item masonry-sm">
                                    <a href="#"><figure><img src="{{ @asset("img/masonry/282x223-03.jpg") }}" class="img-responsive" alt=""></figure></a>
                                    <div class="mas-text">
                                        <div class="post-cat"><a href="#">Fashion</a></div>
                                        <h4 class="mas-title"><a href="#">It is a long <em>established</em> fact that a reader</a></h4>
                                        <div class="mas-details">
                                            <p>There are many variations of passages of Lorem Ipsum available</p>
                                            <a href="#" class="read-more">Read More &#8702;</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.End of masonry item -->
                            </div>
                               
                            @endforeach
                        </div>
                    </div>
                    <div class="col-sm-6 mas-p">
                        <div class="masonry-slide4 owl-carousel owl-theme">
                            @foreach (array_splice($sliders, 14 , 16) as $news)
                            <div class="item">
                                <div class="mas-item masonry-sm">
                                    <a href="#"><figure><img src="{{ @asset("img/masonry/282x223-03.jpg") }}" class="img-responsive" alt=""></figure></a>
                                    <div class="mas-text">
                                        <div class="post-cat"><a href="#">Fashion</a></div>
                                        <h4 class="mas-title"><a href="#">It is a long <em>established</em> fact that a reader</a></h4>
                                        <div class="mas-details">
                                            <p>There are many variations of passages of Lorem Ipsum available</p>
                                            <a href="#" class="read-more">Read More &#8702;</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.End of masonry item -->
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>