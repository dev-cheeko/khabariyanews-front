@extends('layout')
@section('title')
    {{ ucfirst($news->title) }}
@endsection

<!-- /.End of banner content -->
@section('content')
<div class="page-content">
    <div class="container">
        <div class="row">
            <main class="col-sm-8 col-md-9 content p_r_40">
                <div class="details-body">
                    <div class="post_details stickydetails">
                        <header class="details-header">

                            <div class="post-cat">
                                <a href="/category/{{$news->category->id}}/{{$news->category->category_name}}">
                                {{$news->category->category_name}}
                                </a>
                            </div>

                            <h2>{{$news->title}}</h2>
                            <div class="element-block">
                                <div class="entry-meta">
                                    <span class="entry-date"><i class="fa fa-calendar-o" aria-hidden="true"></i><time datetime="{{$news->created_at->format("m/d/Y")}}">{{$news->created_at->format("m/d/Y")}}</time></span> 
                                </div>
                            </div>
                        </header>
                        <div class="gallery_post popup-gallery">
                            <div id="sync1" class="owl-carousel">
                                <div class="item">
                                    <div class="gallery_grid">
                                        <figure class="effect-lily">
                                            <a href="{{ asset("$news->imgUrl")}}" class="gallery_img" title="Title goes here">
                                                <div class="full_skin"><i class="fa fa-arrows-alt" aria-hidden="true"></i></div>
                                                <img src="{{asset("$news->imgUrl")}}" class="img-responsive" alt="{{$news->name}}">
                                            </a>			
                                        </figure>
                                    </div>
                                    <!-- /.End of gallery grid -->
                                </div>
                            </div>
                            <div id="sync2" class="owl-carousel">
                                <div class="item">
                                    <img src="{{ asset("$news->imgUrl") }}" class="img-responsive" alt="{{$news->name}}">
                                </div>
                               
                            </div>
                        </div>
                        <p>
                            {{ $news->description }}
                        </p>
                    </div>
                    <!-- /.End of post details -->
                    <div class="stickyshare">
                        <aside class="share_article">
                            <a href="#" class="boxed_icon facebook" data-share-url="http://mightymedia.nl" data-share-network="facebook" data-share-text="Share this awesome link on Facebook" data-share-title="Facebook Share" data-share-via="" data-share-tags="" data-share-media=""><i class="fa fa-facebook"></i><span>28</span></a>
                            <a href="#" class="boxed_icon twitter" data-share-url="http://mightymedia.nl" data-share-network="twitter" data-share-text="Share this awesome link on Twitter" data-share-title="Twitter Share" data-share-via="" data-share-tags="" data-share-media=""><i class="fa fa-twitter"></i></a>
                            <a href="#" class="boxed_icon google-plus" data-share-url="http://mightymedia.nl" data-share-network="googleplus" data-share-text="Share this awesome link on Google+" data-share-title="Google+ Share" data-share-via="" data-share-tags="" data-share-media=""><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="boxed_icon pinterest" data-share-url="http://mightymedia.nl" data-share-network="pinterest" data-share-text="Share this awesome link on Pinterest" data-share-title="Pinterest Share" data-share-via="" data-share-tags="" data-share-media=""><i class="fa fa-pinterest-p"></i></a>
                            <a href="#" class="boxed_icon comment"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0" y="0" width="14" height="14" viewBox="0 0 14 14" enable-background="new 0 0 14 14" xml:space="preserve"><path d="M3.6 14c0 0-0.1 0-0.1 0 -0.1-0.1-0.2-0.2-0.2-0.3v-2.7h-2.9C0.2 11 0 10.8 0 10.6V0.4C0 0.2 0.2 0 0.4 0h13.3C13.8 0 14 0.2 14 0.4v10.2c0 0.2-0.2 0.4-0.4 0.4H6.9L3.9 13.9C3.8 14 3.7 14 3.6 14zM0.7 10.2h2.9c0.2 0 0.4 0.2 0.4 0.4v2.2l2.5-2.4c0.1-0.1 0.2-0.1 0.2-0.1h6.6v-9.5H0.7V10.2z"></path></svg><span>3</span></a>
                        </aside>
                    </div>
                    <!-- /End of share icon -->
                </div>
                <!-- /.End of details body -->
                <div class="post_related">
                    <h3 class="related_post_title">You Might Also Like...</h3>
                    <div class="row">
                        <div class="col-sm-4">
                            <article class="post_article item_related">
                                <a class="post_img" href="#">
                                    <figure>
                                        <img class="img-responsive" src="assets/img/400x280-7.jpg" alt="">
                                    </figure>
                                </a>
                                <h4><a href="#">Ut et nunc a <em><strong>dolor sodales</strong></em> lacinia quis ac justo.</a></h4>
                            </article>
                            <!-- /.End of related post -->
                        </div>
                        <div class="col-sm-4">
                            <article class="post_article item_related">
                                <a class="post_img" href="#">
                                    <figure>
                                        <img class="img-responsive" src="assets/img/400x280-8.jpg" alt="">
                                    </figure>
                                </a>
                                <h4><a href="#">Aliquam <em><strong>gravida urna</strong></em> ut ipsum hendrerit cursus.</a></h4>
                            </article>
                            <!-- /.End of related post -->
                        </div>
                        <div class="col-sm-4">
                            <article class="post_article item_related">
                                <a class="post_img" href="#">
                                    <figure>
                                        <img class="img-responsive" src="assets/img/400x280-9.jpg" alt="">
                                    </figure>
                                </a>
                                <h4><a href="#">Aliquam eu nunc at nulla efficitur <em><strong>pellentesque a quis.</strong></em></a></h4>
                            </article>
                            <!-- /.End of related post -->
                        </div>
                    </div>
                </div>
                <!-- /.End of  related post -->
                <div class="comments">
                    <h3 class="comment_title">2 Comments</h3>
                    <div class="media">
                        <div class="media-left">
                            <img src="assets/img/img_avatar1.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Jahangir Alom <small>Posted on February 19, 2016</small></h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#" class="btn link-btn btn-rounded">Reply ⇾</a>
                            <div class="media">
                                <div class="media-left">
                                    <img src="assets/img/img_avatar2.png" alt="Demo Avatar Jane Doe" class="media-object">
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">Jane Doe <small>Posted on February 20, 2016</small></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <a href="#" class="btn link-btn btn-rounded">Reply ⇾</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <img src="assets/img/img_avatar1.png" alt="" class="media-object">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">John Doe <small><i>Posted on February 19, 2016</i></small></h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <a href="#" class="btn link-btn btn-rounded">Reply ⇾</a>
                        </div>
                    </div>
                </div>
                <!-- /.End of comment -->
                <div class="comment_form">
                    <h3 class="replay_title">Leave a Reply </h3>
                    <div class="form-group">
                        <textarea class="form-control" id="textarea" rows="5"></textarea>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Name *</label>
                        <div class="col-sm-5">
                            <input class="form-control" id="name2" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email *</label>
                        <div class="col-sm-5">
                            <input class="form-control" id="emai2" type="text">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="website" class="col-sm-2 col-form-label">Website</label>
                        <div class="col-sm-5">
                            <input class="form-control" id="website" type="text">
                        </div>
                    </div>
                    <a href="#" class="btn link-btn">Post Comment ⇾</a>
                </div>
                <!-- /.End of comment content -->
            </main>
            @include('components.sidebar')
        </div>
    </div>
</div>
@endsection