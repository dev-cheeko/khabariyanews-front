@extends('layout')
@section('title')
    {{ $title }}
@endsection
{{-- @section('slider')
    @include('components.slider')
@endsection --}}
@section('content')
<div class="page-content">
    <div class="container">
        <div class="row">
            <main class="col-xs-12 col-sm-8 col-md-8 content p_r_40">
                @if (count($allNews) > 0)    
                    @foreach ($allNews as $news) 
                        @foreach ($news->cateogory as $category) 
                        <div class="media meida-md">
                            <div class="media-left">
                                <a href="/news/{{$news->id}}/{{$news->slug}}">
                                    <img src="{{ @asset( $news->imgUrl ) }}" class="media-object" alt="{{ $news->title }}">
                                </a>
                            </div>
                            <!-- /.Post image -->
                            <div class="media-body">
                                <div class="post-header">
                                    <div class="post-cat"><span>In</span> <a href="/category/{{$category->id}}/{{$category->category_name}}">{{$category->category_name}}</a></div>
                                    <h3 class="media-heading"><a href="/news/{{$news->id}}/{{$news->slug}}">{{$news->title}}</a></h3>
                                    <div class="entry-meta">
                                        <span class="entry-date"><i class="fa fa-calendar-o" aria-hidden="true"></i><time datetime="{{$news->created_at->format("m/d/Y")}}">{{$news->created_at->format("m/d/Y")}}</span> 
                                    </div>
                                    <!-- /.Post meta -->
                                </div>
                                <div class="element-block">
                                    <a href="/news/{{$news->id}}/{{$news->slug}}" class="btn link-btn btn-outline btn-rounded">Reading &#8702;</a>
                                    <!-- /.Post button -->
                                    <div class="post_share">
                                        <a class="smedia facebook fa fa-facebook" href="https://www.facebook.com/sharer/sharer.php?u={{URL::to("/")}}/news/{{$news->id}}/{{$news->slug}}"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @endforeach
                @else
                    <p class="alert alert-danger">No result found</p>
                @endif
                <ul class="pagination">
                    @if (count($allNews) > 0)
                    {{ $allNews->links() }}
                    @endif
                </ul>
                <!-- /.End of pagination -->
            </main>
            @include('components.sidebar')
        </div>
    </div>
</div>
@endsection