<div class="container">
    <div class="newstricker_inner">
        <div class="trending"><strong>Trending</strong> Now</div>
        <div id="newsTicker" class="owl-carousel owl-theme">
            @foreach ($latestNewsTicker as $item)
            <div class="item">
                <a href="/news/{{$item->id}}/{{$item->slug}}">{{ ucfirst($item->title) }}</a>
            </div>
            @endforeach
        </div>
    </div>
</div>