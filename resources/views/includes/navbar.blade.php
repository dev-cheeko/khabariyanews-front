<nav class="navbar navbar-inverse navbar-sticky navbar-mobile bootsnav">
    <div class="container">
        <div class="attr-nav">
            <ul>
                <li id="btn-search1"><a href="javascript:void(0)" id="btn-search2"><i class="fa fa-search"></i></a></li>
            </ul>
        </div>
        <!-- End Atribute Navigation -->
        <!-- Start Header Navigation -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="/"><img src="{{ @asset("/img/logo.png") }} " class="logo" alt="Khabariya news official Logo"></a>
        </div>
        <!-- End Header Navigation -->
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="nav navbar-nav navbar-center" data-in="navFadeInDown" data-out="navFadeOutUp">
                <li class="@if(URL::to("/")) active @endif"><a href="/">Home</a></li>
                @foreach ($menu as $item)
                    <li class=""><a href="/news-show/{{$item->slug}}">{{$item->parent_cat_name}}</a></li>
                @endforeach
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>   
   
    <!-- /. End of side menu -->
    <div class="side-overlay"></div>
</nav>
<div class="clearfix"></div>
<!-- /.End of navigation -->
<div class="search">
    <button id="btn-search-close" class="btn btn--search-close" aria-label="Close search form"> <i class="ti-close"></i></button>
    <form class="search__form" action="javascript:void(0)" method="post">
        <input class="search__input" name="search" type="search" placeholder="Search and hit enter..."/>
        <span class="search__info">Hit enter to search or ESC to close</span>
    </form>
    
</div>