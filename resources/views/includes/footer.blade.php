<footer class="footer-black">
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-4">
                   <div class="footer-box">
                        <h3 class="widget-title title-white">Section</h3>
                        <ul class="footer-cat">
                           <li><a href="/news-show/sports">Sports</a></li>
                           <li><a href="#">International</a></li>
                           <li><a href="/news-show/corona">Corona</a></li>
                           <li><a href="">City</a></li>
                           <li><a href="/news-show/startup">Startup</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-md-2">
                    <div class="footer-box">
                        <h3 class="widget-title title-white">Need help</h3>
                        <ul class="footer-cat">
                            <li><a href="#">Our Community</a></li>
                            <li><a href="#">See Our Portfolio</a></li>
                            <li><a href="#">Story About Us</a></li>
                            <li><a href="#">Keep in Touch</a></li>
                            <li><a href="#">Purchase Our Products</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3">
                    <div class="footer-box">
                        <h3 class="widget-title title-white">Latest News</h3>
                     
                    </div>
                </div>
                <div class="col-sm-3 col-md-2">
                    <div class="footer-box">
                        <h3 class="widget-title title-white">Follow us</h3>
                        <ul class="footer-cat top-socia-share">
                            <li><a href="https://www.facebook.com/NewsKhabariya/"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-youtube-play"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-footer">
        Copyright © 2020. All Rights Reserved.
    </div>
</footer>
<!-- /.End of footer -->