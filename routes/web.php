<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'NewsArticleController@news');
Route::get('news/{id}/{slug?}' , 'NewsArticleController@showNews');
Route::get('news-show/{slug}' , 'NewsArticleController@newsPtShow');
