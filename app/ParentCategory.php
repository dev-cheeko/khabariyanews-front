<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
class ParentCategory extends Model
{
    public function categories() {
        return $this->hasMany(Category::class , 'parent_cat_id' , 'id');
    }
}
