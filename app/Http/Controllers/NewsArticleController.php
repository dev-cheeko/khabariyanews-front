<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  
use App\News;
use App\Category;
use App\ParentCategory;

class NewsArticleController extends BaseController
{
    // Table 
    protected $newsTable    = 'news';
    
    public function politicsContent() {
        $politics = Category::with('news')->limit('5')->get();
        return $politics;
    }

    //Latest Articles
    public function news() {
        
        $perPage = 5;
        $news = News::with('category')
        ->where([
            ['news_available','=' ,'1' ],
            ['archive','=' ,'0' ]
        ])
        ->simplePaginate($perPage);
        // return dd($this->sliderData());
        return view('pages.home' , [ 
            'allNews' => $news , 
            'latestNewsTicker' => $this->tickerNews,
            'sliders' => $this->sliderData(),
            'menu'  => $this->menu,
            'latestNews' => $this->latestNews()
        ]);
    }

    public function showNews($id , $slug="") {
        $news = News::with('category')->where([
            ['news_available','=' ,'1' ],
            ['archive','=' ,'0' ]
        ])->findOrFail($id);
        
        return view('pages.single-news' , [
            'news' => $news , 
            'latestNewsTicker' => $this->tickerNews,
            'menu'  => $this->menu,
            'latestNews' => $this->latestNews()
        ]);
    }

    // Return all the news related to the category which related to the parent
    public function newsPtShow( $slug) {
        $perPage = 5; 
        $parentCategory = ParentCategory::where('slug' , $slug)->first();
        
        if(Category::where('parent_cat_id' , $parentCategory->id)->exists()) {
            $categories = Category::where('parent_cat_id' , $parentCategory->id)->pluck('id');
            $news = News::with('category')->where([
                ['category_id', '=', $categories],
                ['news_available','=' , '1' ],
                ['archive', '=' , '0' ]
            ])->simplePaginate($perPage);
            // return dd($categories); 
            return view('pages.news-show-by-menu' , [
                'title'   => $parentCategory->parent_cat_name,
                'allNews' => $news,
                'latestNewsTicker' => $this->tickerNews,
                'menu'  => $this->menu,
                'latestNews' => $this->latestNews()
            ]);
        } else {
            $news = [];
            return view('pages.news-show-by-menu' , [
                'title'   => $parentCategory->parent_cat_name,
                'allNews' => $news,
                'latestNewsTicker' => $this->tickerNews,
                'menu'  => $this->menu,
                'latestNews' => $this->latestNews()
            ]);
        }

        // return abort(404);
        
    }


    public function latestNews() {
        $latestnews = News::with('category')->orderBy('id' , 'desc')->limit('5')->get();
        return $latestnews;
    }
    
}
