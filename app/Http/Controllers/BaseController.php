<?php

namespace App\Http\Controllers;
use App\News;
use App\Category;
use App\ParentCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class BaseController extends Controller
{
    protected $tickerNews;
    public $menu;

    public function __construct()
    {
      $this->tickerNews = $this->newsTicker();    
      $this->menu = $this->parentCategory();    
    }

    public function newsTicker() {
        $news = DB::table($this->newsTable)->orderBy('id', 'desc')->limit(5)->get();
        return $news;
    }


    public function parentCategory() {
        $parentCat = ParentCategory::orderBy('seq' ,'asc')->get();
        return $parentCat;
    }

    public function sliderData() {
        $news = News::with('category')->where([
            ['news_available','=' , '1' ],
            ['archive', '=' , '0' ]
        ])->orderBy('id' , 'desc')->limit(16)->get();
        return $news->toArray();
    }
}
