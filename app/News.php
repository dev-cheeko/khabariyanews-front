<?php

namespace App;

use App\Category;

use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\View;
class News extends Model 
{
   
    protected $dates= ['published_at' , 'created_at'];
    
    public function category() {
        return $this->belongsTo(Category::class , 'category_id' , 'id');
    }

    
}
